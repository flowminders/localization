<?php
namespace FlowControl\Localization;

use FlowControl\Localization\Models\Language;
use Illuminate\Support\Facades\Facade;

class LanguageFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'flowcontrol.language'; }

    public static function hasLanguages()
    {
        return Language::all()->isEmpty();
    }

    public static function getLanguages()
    {
        try {
            return Language::where('code', '!=', App::getLocale())->get();
        } catch (\Exception $e) {
            return [];
        }
    }

    public static function getLanguageCodes()
    {
        try {
            return Language::pluck('code')->toArray();
        } catch (\Exception $e) {
            return [];
        }
    }

    public static function getLanguageId($lang)
    {
        try {
            return Language::where('code', $lang)->pluck('id')->first();
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function getLanguageCode($id)
    {
        try {
            return Language::where('id', $id)->pluck('code')->first();
        } catch (\Exception $e) {
            return null;
        }
    }
}