<?php

if( !function_exists('router') ) {

    function router($name, $params = [])
    {
        return Localizator::route($name, $params);
    }

}

// Almost same as change_locale(). Duplicate?
if( !function_exists('change_locale') ) {

    function change_locale($locale) {
        $route = Route::current();

        //in_array('flowcontrol.localized', $route->middleware())
        if (null == ($route && $route->getName())) {
            return url('/', $locale);
        }

        $oldParams = $route->parameters();
        unset($oldParams['lang']);

        $params = array_merge([$locale], $oldParams);

        return route($route->getName(), $params);
    }

}

if ( !function_exists('get_languages') ) {

    function get_languages()
    {
        return Language::getLanguages();
    }
}

if ( !function_exists('get_language_codes') ) {

    function get_language_codes()
    {
        return Language::getLanguageCodes();
    }
}

if ( !function_exists('get_language_id') ) {

    function get_language_id($lang)
    {
        return Language::getLanguageId($lang);
    }
}

if ( !function_exists('get_language_code') ) {

    function get_language_code($id)
    {
        return Language::getLanguageCode($id);
    }
}