<?php

namespace FlowControl\Localization\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'flowcontrol_languages';
    protected $fillable = ['name', 'code'];
}
